#include <stdlib.h>
#include <sepol/policydb/avtab.h>
#include <sepol/policydb/policydb.h>
#include <sepol/errcodes.h>

static inline 
int avtab_hash(struct avtab_key *keyp, uint16_t mask)
{
        return ((keyp->target_class + (keyp->target_type << 2) +
                 (keyp->source_type << 9)) & mask);
}

static void 
avtab_delete_node(avtab_t * h, int hvalue, avtab_ptr_t prev, avtab_ptr_t node)
{
        if (prev) {
		prev->next = node->next;
	}
	else {
		h->htable[hvalue] = NULL;
	}

	free(node);

        h->nel--;
}

int avtab_remove(avtab_t * h, avtab_key_t * key)
{
        int hvalue;
        avtab_ptr_t prev, cur;
        uint16_t specified =
            key->specified & ~(AVTAB_ENABLED | AVTAB_ENABLED_OLD);

        if (!h || !h->htable)
                return SEPOL_ENOMEM;

        hvalue = avtab_hash(key, h->mask);
        for (prev = NULL, cur = h->htable[hvalue];
             cur; prev = cur, cur = cur->next) {
                if (key->source_type == cur->key.source_type &&
                    key->target_type == cur->key.target_type &&
                    key->target_class == cur->key.target_class &&
                    (specified & cur->key.specified))
                        break;
                
		if (key->source_type < cur->key.source_type)
                        return -1;
                if (key->source_type == cur->key.source_type &&
                    key->target_type < cur->key.target_type)
                        return -1;
                if (key->source_type == cur->key.source_type &&
                    key->target_type == cur->key.target_type &&
                    key->target_class < cur->key.target_class)
                        return -1;
        }

        avtab_delete_node(h, hvalue, prev, cur);
       
        return 0;
}
