PREFIX ?= $(DESTDIR)/usr
BINDIR ?= $(PREFIX)/bin
LIBDIR ?= $(PREFIX)/lib

CFLAGS ?= -g -Wall -Werror -Wshadow -O2 -pipe -fno-strict-aliasing
LDLIBS=$(LIBDIR)/libsepol.a 

all: sepolicy-inject sepolicy-remove

sepolicy-inject: sepolicy-inject.c tokenize.c

sepolicy-remove: sepolicy-remove.c tokenize.c avtab_remove.c

clean:
	rm -f sepolicy-inject sepolicy-remove
